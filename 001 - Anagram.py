"""
Author : RyuCoder
Purpose : Algorithm to check if two words are anagrams of each other 
"""
import collections


def print_results(word1, word2, result):
    messages = {
                "anagram" : "{} and {} are anagrams of each other.",
                "length_mismatch": "{} and {} can't be anagarams of each other as their lengths does not match.",
                "letter_mismatch": "{} and {} can't be anagarams of each other as some letters are not present in both the words.",
                "word_count_mismatch": "{} and {} can't be anagarams of each other as some of the letters are not present in both the words in the same quantity."
             }

    # check the result and print the right statement
    if result in messages:
        print(messages[result].format(word1, word2))

    print("")
    

def pre_process_words(word1, word2):
    """ Preprocesses the word1 and word2. 

    Args: 
        word1, word2. Both are string type.

    Returns: 
        word1 and word2    
    
    """

    word1 = str(word1)
    word2 = str(word2)

    return word1, word2


def check_anagram(word1, word2):
    """ Checks if the word1 and word2 are anagrams of each other. 

    Args: 
        word1, word2. Both are string type.

    Returns: 
        result keyword that will be used to print the output.
    
    """
    set1 = set(word1)
    set2 = set(word2)
    counter1 = collections.Counter(word1)
    counter2 = collections.Counter(word2)

    if len(word1) == len(word2):
        # check if the distinct letters are present in both the words, 
        # if not return False and the reason keyword
        for letter in set1:
            if letter not in set2:
                return "letter_mismatch"

        # check if the count of each letter is same in both the words
        # if not return False and the reason keyword
        for letter in list(counter1):
            if counter1[letter] != counter2[letter]:
                return "word_count_mismatch"
        
        # If all the checks pass, two words are anamgrams
        return "anagram"

    else:
        return "length_mismatch"


def get_user_inputs():
    same_flag = True

    print("")
    print("Algorithm to check if two words are anagrams of each other")
    print("")

    while same_flag == True:
        print("Please note that the first word and second word can not be the same.")
        print("")
        word1 = input("Please input the first word: \n")
        print("")
        word2 = input("Please input the second word: \n")
        print("")

        if word1 != word2:
            same_flag = False

    return word1, word2


def main():
   
    word1, word2 = get_user_inputs()

    # convert both the words to string type
    word1, word2 = pre_process_words(word1, word2)

    result = check_anagram(word1, word2)

    print_results(word1, word2, result)


if __name__ == "__main__":
    main()


"""
Problems with the code:
1. Code is not checking if the words are real or imaginary, 
    could write a function for that in future if required.
2. Lowercase and Uppercase scenario is not considered 
    e.g. Love and love are not anagrams
"""
