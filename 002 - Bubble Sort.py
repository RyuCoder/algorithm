"""
Author : RyuCoder
Purpose : Algorithm to Bubble Sort a list 
"""


def print_sorted_list(list):
    print("")
    print(list)
    print("")


def bubble_sort(list):
    """ Sorts a list using bubble sort algorithm 

    Args: 
        List thats needs to be sorted.

    Returns: 
        Sorted list
    
    """

    for i in range(0, len(list)):
        for i in range(0, len(list)-1):
            if list[i] > list[i+1]:
                list[i], list[i+1] = list[i+1], list[i]
        
    return list


def main():
    list = [5, 1, 4, 2, 8]

    sorted_list = bubble_sort(list)

    print_sorted_list(sorted_list)


if __name__ == "__main__":
    main()


"""
Problems with the code:

"""
