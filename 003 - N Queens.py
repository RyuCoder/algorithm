"""
Author : RyuCoder
Website: http://ryucoder.in/
Purpose : Algorithm to solve the N Queens problem 
"""


from pprint import pprint as pp
from random import randint


# if no of queens placed on board are equal to no_of_queens, queen_of_queens is set to True
queen_of_queens = False 
no_of_queens = 9
board = [ [x for x in range(no_of_queens)] for y in range(no_of_queens)]
queen = "Queen"

# yes and no variables are used instead of "Yes" and "No" 
# to apply the format to the output in terminal
yes = "Yeso"
no = "Nooo"


def print_results():
    print()
    if queen_of_queens:
        print("N Queens problem was solved perfectly.")
    else:
        print("N Queens problem was not solved perfectly.")
    print()

    pp(board)


def board_has_queens():
    count = 0

    for i in range(no_of_queens):
        if queen in board[i]:
            count += 1
    
    return count


def update_board(place):
    # print("Place = {} from update board.".format(place))
    x = place[0]
    y = place[1]
    # range_list = [0, 1, 2, 3, 4, 5, 6, 7, 8]

    # horizental setting of no
    for i in range(no_of_queens):
        if board[x][i] == yes:
            board[x][i] = no
    
    # vertical setting of no
    for i in range(no_of_queens):
        if board[i][y] == yes:
            board[i][y] = no


    # for right - bottom diagonal
    index = 1
    for x_index in range(x+1, no_of_queens):
        # print(x_index)  
        for y_index in range(y+1, no_of_queens):
            # print(y_index)

            if x_index-index == x and y_index-index == y:
                
                if board[x_index][y_index] == yes: 
                    board[x_index][y_index] = no

        index += 1


    # for left - top diagonal
    index = 1
    for x_index in range(x-1, -1, -1):
        for y_index in range(y-1, -1, -1):

            if x_index + index == x and y_index + index == y:
                
                if board[x_index][y_index] == yes:
                    board[x_index][y_index] = no
    
        index += 1


    # for right - top diagonal
    index = 1
    for x_index in range(x - 1, -1, -1):
        for y_index in range(y+1, no_of_queens):

            if x_index + index == x and y_index - index == y:

                if board[x_index][y_index] == yes:
                    board[x_index][y_index] = no

        index += 1


    # for left - bottom diagonal
    index = 1
    for x_index in range(x+1, no_of_queens):
        for y_index in range(y-1, -1, -1):

            if x_index - index == x and y_index + index == y:

                if board[x_index][y_index] == yes:
                    board[x_index][y_index] = no

        index += 1


def place_queen(place):
    # print("Place = {} from place queen".format(place))
    x = place[0]
    y = place[1]
    
    if board[x][y] == yes:
        board[x][y] = queen
    
    # print("Queen placed.")


def _get_random_int():
    """ randint(a, b) gives a random integer inclusive of the value of b """
    return randint(0, no_of_queens-1)


def choose_random_place_for_queen():
    """ Code will still check for places which are not required, need to fix this """
    """ row which already have queen should not be checked """
    """ y is not random """

    x = _get_random_int()
    y = _get_random_int()

    while(queen in board[x]):
        x = _get_random_int()

    for i in range(no_of_queens):
        if board[x][i] == yes:
            y = i 
            break
    # print([x, y])
    return [x, y]


def iterate_over_board():
    for i in range(no_of_queens):
        place = choose_random_place_for_queen()
        place_queen(place)
        update_board(place)


def generate_board(no_of_queens):
    
    for x in range(no_of_queens):
        for y in range(no_of_queens):
            board[x][y] = yes


def main():
    global queen_of_queens

    generate_board(no_of_queens)
    
    iterate_over_board()

    board_queens = board_has_queens()
    
    if board_queens == no_of_queens:
        queen_of_queens = True
    else:
        """ These two lines are only printed when the N queen problem was not solved. """
        print()
        print("board_queens")
        print(board_queens)
        print()

    print_results()


if __name__ == "__main__":
    main()


